var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index.html');
});

router.post('/users', function(req, res, next) {
  var name = req.body.firstname;
  res.send("O nome é: " + name);
});

module.exports = router;
